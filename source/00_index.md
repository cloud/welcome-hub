---
title: Home
permalink: /
layout: default
---

We are currently migrating all __G1 MetaCentrum__ workloads to the new [__G2 MetaCentrum Cloud__](https://brno.openstack.cloud.e-infra.cz/). 
More information about the migrations can be found [here](https://docs.e-infra.cz/compute/openstack/migration-to-g2-openstack-cloud/). 
The current status of the [G1-G2 cloud migrations](https://docs.google.com/document/d/1taM5cW-khRFu7k-WU4Fz9wLnw_EVqBYRFTuY6p5lpRQ/view?tab=t.0#heading=h.oh6irb3i05w9).

__G1 MetaCentrum Cloud__ offers a flexible computing and storage environment
suitable for a wide variety of applications. These include, but are not
limited to, web-based services and portals, container hosts, HPC worker
nodes, CI/CD worker nodes, database hosts, and [numerous complex scientific use cases](/use-cases/).

<div class="button-container">
  <div class="button-container-column">
    <a href="https://dashboard.cloud.muni.cz/" class="dashboard-button">Dashboard (Horizon UI)</a>
    <a href="https://brno.openstack.cloud.e-infra.cz/" class="dashboard-button">G2 MetaCentrum Cloud</a>
  </div>
  <div class="button-container-column">
    <a href="https://docs.e-infra.cz/compute/openstack/" id="einfra-documentation-button" class="documentation-button">
      <div class="margin-auto">Documentation</div>
    </a>
  </div>
</div>
  
The __G1 MetaCentrum Cloud__ is operated by MetaCentrum, CESNET department responsible for coordination of computing services, on behalf of e-INFRA CZ.   
Partners involved in the development of the __G1 MetaCentrum Cloud__, providing computing resources and using the cloud service:
* **Institute of Computer Science of Masaryk University**,
* and other [partners](/partners/) representing national and international user communities.

__G1 MetaCentrum Cloud__ is accessible to
* **all users of the Czech e-infrastructure for science, development, and education**,https://docs.cloud.muni.cz/cloud/register/
* selected national and international [user communities](/partners/).

Access and usage of __G1 MetaCentrum Cloud__ services is subject of [terms and conditions](https://docs.cloud.muni.cz/cloud/terms-of-service/).

For registration, refer to [Documentation - How To Get Access](https://docs.cloud.muni.cz/cloud/register/).


