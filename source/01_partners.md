---
title: Partners
permalink: /partners/
layout: default
---

### Partners
Partners involved in the development of the __G1 MetaCentrum Cloud__, providing computing resources and using the cloud service.

| <img src="{{ "/assets/metacentrum-logo.png" | relative_url }}" width="210" />     	| [https://www.metacentrum.cz/](https://www.metacentrum.cz/en/)	|
| <img src="{{ "/assets/ics-logo.png" | relative_url }}" width="100" />              	| [https://www.ics.muni.cz/](https://www.ics.muni.cz/en/)  	|
| <img src="{{ "/assets/logo-cerit-sc.png" | relative_url }}" width="128" />         	| [https://www.cerit-sc.cz/](https://www.cerit-sc.cz/?lang=en)  	|
| <img src="{{ "/assets/elixir_czechrepublic.png" | relative_url }}" width="110" /> 	| [https://www.elixir-europe.org/](https://www.elixir-europe.org/about-us/who-we-are/nodes/czech-republic)  	|
| <img src="{{ "/assets/c4ew.png" | relative_url }}" width="100" />                 	| [https://c4e.cz/](https://c4e.cz/?lang=en) <br/> [https://www.kypo.cz/](https://www.kypo.cz/en)  	|
| <img src="{{ "/assets/EOSC_logo.png" | relative_url }}" width="230" />              | [https://www.eosc-hub.eu/](https://www.eosc-hub.eu/)    |
| <img src="{{ "/assets/EGI.png" | relative_url }}" width="230" />                    | [https://www.egi.eu/](https://www.egi.eu/)    |
| <img src="{{ "/assets/recetox.png" | relative_url }}" width="148" />                | [http://www.recetox.muni.cz/](http://www.recetox.muni.cz/index-en.php)    |
| <img src="{{ "/assets/panosc.png" | relative_url }}" width="220" />                 | [https://panosc-eu.github.io/](https://panosc-eu.github.io/)    |
