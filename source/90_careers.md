---
title: Careers
permalink: /careers/
layout: default
---

<h3>Status: <div style="color: #008024; display: inline">We Are Hiring</div></h3>

**Currently, we are looking for a:**

- [Cloud Engineer](https://www.cesnet.cz/sdruzeni/kariera/cloud-engineer/) (part-time, full time)
