# Welcome Hub
Docker container with nginx for the [MetaCentrum Cloud Welcome Page](https://cloud.muni.cz/).

New version of container is deployed via CI on [MUNI ICS cPanel](https://web01.ics.muni.cz:2083).
