FROM nginx:stable-alpine

LABEL maintainer="MetaCentrum Cloud Team <cloud@ics.muni.cz>" \
      version="0.0.1" \
      description="MetaCentrum Cloud Welcome Hub"

# Insert content from pre-built artifact folder
COPY public /usr/share/nginx/html

# Make default configurable via ENV VARS
COPY default.template /etc/nginx/conf.d/default.template

# Render template on start
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"]
